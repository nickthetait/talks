size = 2;

color( "red", 1.0 )
{
    rotate([90,0,0])
    {
        linear_extrude(height = 10, twist = 180)
        {
            square(size);
        }
    }
}