$fn = 100;

size = 5;
cylinder(size);

translate([size*2, 0, 0])
{
    cylinder(1, size, size);
}