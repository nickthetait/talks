$fn = 200;

module support(
    length,
    width,
    thickness
)
{
    //Horizontal
    cube(size = [length, width, thickness], center = false);

    //Vertical
    cube(size = [thickness, width, length], center = false);
}

module brace(
    length,
    width,
    thickness
)
{
    difference()
    {
        translate([thickness, 0, thickness])
            cube(size = [length - thickness, width, length - thickness], center = false);
        rotate([0, 45, 0])
            translate([-length, -1, sqrt(2) * (length + thickness) / 2])
                cube(size = [length * 2, width + 2, length * 2], center = false);
    }
}

module peg(
    width,
    thickness,
    peg_diameter,
    peg_depth
)
{
    translate([-peg_depth, width/2, peg_diameter/2])
        rotate([0, 90, 0])
            cylinder(r = peg_diameter/2, h = peg_depth, center = false);
}

module main(
    length = 15,
    width = 12,
    thickness = 2,
    peg_diameter = 6,
    peg_depth = 5.5,
    shelf_height = 13.5,
    brace_type = "single",
    cutout_type = "line"
)
{
    support(length, width, thickness);
    brace(length, width, thickness);
    peg(width, thickness, peg_diameter, peg_depth);
}

main();