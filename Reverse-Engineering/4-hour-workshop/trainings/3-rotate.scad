$fn = 100;

size = 1;

translate([0, 0, 0])
{
    cylinder(size);
}

translate([size*3, 0, 0])
{
    rotate([45, 0, 0])
    {
        color("lightgreen")
        cylinder(size);
    }
}

translate([size*6, 0, 0])
{
    rotate([0, 90, 0])
    {
        color("pink")
        cylinder(size);
    }
}