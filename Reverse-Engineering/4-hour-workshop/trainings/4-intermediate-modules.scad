$fn = 200;

module simple_dice
(
    scale = 10,
    shade = "pink"
)
{
    color(shade)
    {
        intersection()
        {
            cube(size=scale, center=true);
            sphere(r=scale * .7, center=true);
        }
    }
}

scale = 10;
!simple_dice(scale, "teal");





//------------------------------------------------------------------------------





module simple_dice_with_translate
(
    scale,
    shade,
    move = [0,0,0]
)
{
    translate(move)
    {
        simple_dice(scale, shade);
    }
}

simple_dice_with_translate(scale * 2, "pink", [scale * 3, 0, 0]);
simple_dice_with_translate(scale * .5, "orange", [scale * 6, 0, 0]);