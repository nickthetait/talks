$fn = 200;

module roundedCube
(
    radius = 5,
    length = 10
)
minkowski()
{
    sphere(r=radius);
    cube(size=length);
}

roundedCube
(
    radius = 2,
    length = 10
);