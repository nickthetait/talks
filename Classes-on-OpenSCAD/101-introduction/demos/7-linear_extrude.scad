$fn = 100;

//linear_extrude(
//    height = distance,
//    center = true,
//    twist = angle)
//{...}

size = 2;

linear_extrude(height = 10, twist = 0)
{
    square(size);
}

translate([size*3,0,0])
{
    linear_extrude(height = 10, twist = 180)
    {
        square(size, center = true);
    }
}