number = 4;

//Variables only ever hold a single value and it cannot be changed
number = 10; //This assignment occurs later and therfore "wins"

calculation = cos(number*3) + 12;

string = "Some text";

boolean = false;