count = 60;

color("green")
for(i=[0:count])
{
    height = sin(i*10)*40+60;
    translate([i,0,0])
        cube(size=[1, 1, height]);
}

color("orange")
for(i=[0:count])
{
    height = cos(i*10)*40+60;
    translate([i,0,120])
        cube(size=[1, 1, height]);
}

color("red")
for(i=[0:count])
{
    height = tan(i*10)*20+60;
    translate([i,0,250])
        cube(size=[1, 1, height]);
}