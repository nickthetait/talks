$fn = 50;

color("lightgreen")
{
    circle(r=5);
    #square(size=10);
}

translate([30, 0, 0])
{
    
    difference()
    {
        circle(r=5);
        square(size=10);
    }
}