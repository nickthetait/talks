//Example of the problem, on the left
translate([0,0,0])
{
    difference()
    {
        cube(20);
        cube(10);
    }
}











































//Possible solution, on the right
translate([100,0,0])
{
    difference()
    {
        cube(20);
        translate([-1,-1,-1])
        {
            cube(11);
        }
    }
}