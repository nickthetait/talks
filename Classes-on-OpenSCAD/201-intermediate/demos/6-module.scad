/*
module name( parameters... )
{
    create objects here
}
*/

module test
(
    scale = 10
)
{
    difference()
    {
        cube(size=scale, center = true);
        sphere(r=scale * .7,center=true);
    }
}

test();

translate([50,0,0])
{
    test(40);
}