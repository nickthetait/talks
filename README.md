# Talks, Presentations, Classes & Workshops

* Series of presentations I have given on various topics

* Pull requests will probably be ignored

# OpenSCAD Training

* This is a series of classes on the software OpenSCAD

* Read [here](http://www.openscad.org/) to learn about what the software is for

# Reverse Engineering

* Developed for [Maker Faire Denver 2017](https://denver.makerfaire.com/maker/entry/128/)
* Expanded into a 4 hour workshop for [DEF CON 26](https://defcon.org/html/defcon-26/dc-26-workshops.html#tait), combining content from my OpenSCAD classes
