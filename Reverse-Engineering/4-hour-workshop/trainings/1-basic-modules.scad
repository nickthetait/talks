module cubeColor(scale, shade)
{
    color(shade)
    cube(size=scale);
}

scale = 5;
cubeColor(scale, "teal");

translate([2*scale, 0, 0])
{
    cubeColor(4*scale, "pink");
}