$fn = 200;

module peg(
    peg_diameter = 6,
    peg_depth = 5.5
)
{
    cylinder(r = peg_diameter/2, h = peg_depth, center = false);
}

peg();