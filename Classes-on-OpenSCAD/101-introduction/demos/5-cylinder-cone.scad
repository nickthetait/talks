$fn = 50;

//cylinder(
//    h = height,
//    r1 = BottomRadius,
//    r2 = TopRadius,
//    center = true/false);

cylinder
(
    h = 5,
    r = 2
);

translate([10,0,0])
{
    cylinder
    (
        h = 5,
        r1 = 2,
        r2 = 0 //This creates a cone
    );
}