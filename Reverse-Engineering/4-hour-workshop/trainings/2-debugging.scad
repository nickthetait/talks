size = 4;
offset = size + 2;

// Use # or % highlight
#translate([0, 0, 0])
    cube(size);

%translate([offset, 0, 0])
    cube(size);


// Use ! to ignore everything else
translate([0, offset, 0])
    cube(size);


translate([0, 0, offset])
    cube(size);


//echo() shows the value of a variable in the console panel ->
echo("Size is equal to ", size);
echo("Offset is equal to ", offset);