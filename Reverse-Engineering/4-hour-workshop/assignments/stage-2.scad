$fn = 200;

module support(
    length,
    width,
    thickness
)
{
    //Horizontal
    cube(size = [length, width, thickness], center = false);

    //Vertical
    cube(size = [thickness, width, length], center = false);
}

module peg(
    width,
    thickness,
    peg_diameter,
    peg_depth
)
{
    translate([peg_diameter/2, width/2, -peg_depth])
        cylinder(r = peg_diameter/2, h = peg_depth, center = false);
}

length = 15;
width = 12;
thickness = 2;
peg_diameter = 6;
peg_depth = 5.5;
support(length, width, thickness);
peg(width, thickness, peg_diameter, peg_depth);