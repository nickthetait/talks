simpleVector = [1,2,3];

x = 1;
y = 2;
z = 3;
movement = [x, y, z];




//------------------------------------------------------------------------------




size = 4;
offset = size + 2;

translate([0, 0, 0]) //No movement, stay at origin
{
    color("red")
    cube(size);
}

translate
(
    [
        offset, //Move along X axis
        0,
        0
    ]
)
{
    color("lightblue")
    cube(size=[size/4, size, size*2]);
}

translate
(
    [
        0,
        offset, //Move along Y axis
        0
    ]
)
{
    color("green")
    cube(size=[size*4, size, size]);
}

translate
(
    [
        0,
        0,
        offset //Move along Z axis
    ]
)
{
    color("orange")
    cube(size=[size, size*2, size/2]);
}
