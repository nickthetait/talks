//translate([
//    dist_x,
//    dist_y,
//    dist_z])
//{...}

size = 4;
offset = size + 2;

translate([0,0,0]) //No movement, stay at origin
{
    cube(size);
}

translate
(
    [
        offset, //Move along X axis
        0,
        0
    ]
)
{
    cube(size);
}

translate
(
    [
        0,
        offset, //Move along Y axis
        0
    ]
)
{
    cube(size);
}

translate
(
    [
        0,
        0,
        offset //Move along Z axis
    ]
)
{
    cube(size);
}

translate
(
    [
        offset, //Move along all 3 axes
        offset,
        offset
    ]
)
{
    cube(size);
}