//name = data;

number = 4;
cube(number);

//Variables only ever hold a single value and it cannot be changed
//in other languages these would be called constants
number = 10; //This assignment occurs later and "wins"

calculation = cos(number*3) + 12;

string = "Some text";

boolean = false;

vector = [1,2,5];