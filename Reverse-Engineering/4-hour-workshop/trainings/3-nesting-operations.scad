size = 2;
move = [10, 10, 10];
turn = [90, 0, 0];

//Compact style
translate(move) rotate(turn) color("grey") square(size);

//Indentation style
translate(move)
    rotate(turn)
        color("grey")
            square(size);

//Verbose style
translate
(
    move
)
{
    rotate
    (
        turn
    )
    {
        color
        (
            "grey"
        )
        {
            square
            (
                size
            );
        }
    }
}