$fn = 100;

//rotate([
//    deg_x,
//    deg_y,
//    deg_z
//    ])
//{ ... }

size = 1;

rotate([45,0,0])
{
    cylinder(size);
}

translate([size*3,0,0])
{
    cylinder(size);
}