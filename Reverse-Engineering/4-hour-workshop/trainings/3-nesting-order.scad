$fn = 50;

color("orange")
{
    difference()
    {
        circle(r=5);
        circle(r=4);
    }
    square(size=10);
}

color("lightgreen")
translate([30, 0, 0])
{
    difference()
    {
        union()
        {
            circle(r=5);
            square(size=10);
        }
        circle(r=4);
    }
}