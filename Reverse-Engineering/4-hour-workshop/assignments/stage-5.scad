$fn = 200;

module support(
    length,
    width,
    thickness
)
{
    //Horizontal
    cube(size = [length, width, thickness], center = false);

    //Vertical
    cube(size = [thickness, width, length], center = false);
}

module single_brace(
    length,
    width,
    thickness,
    brace_width,
    horiz_offset = 0
)
{
    translate([thickness, horiz_offset, thickness])
        cube(size = [length - thickness, brace_width, length - thickness], center = false);
}

module brace_type(
    length,
    width,
    thickness,
    brace_type = "single",
)
{
    type = "full";
    difference()
    {
        if(brace_type == "single")
        {
            brace_width = thickness;
            single_brace(length, width, thickness, brace_width, horiz_offset = width/2 - brace_width/2);
        }
        else if(brace_type == "double")
        {
            single_brace(length, width, thickness, thickness);
            single_brace(length, width, thickness, thickness, horiz_offset = width - thickness);
        }
        else if(brace_type == "full")
        {
            single_brace(length, width, thickness, width);
        }
        else
        {
            echo("Unknown brace type");
        }

        rotate([0, 45, 0])
            translate([-length, -1, sqrt(2) * (length + thickness) / 2])
                cube(size = [length * 2, width + 2, length * 2], center = false);
    }
}

module peg(
    width,
    thickness,
    peg_diameter,
    peg_depth
)
{
    translate([-peg_depth, width/2, peg_diameter/2])
        rotate([0, 90, 0])
            cylinder(r = peg_diameter/2, h = peg_depth, center = false);
}

module catch(
    width,
    thickness,
    shelf_height,
    catch_angle = 10
)
{
    catch_lenght = 20;
    height_offset = thickness * sin(catch_angle);
    total_height = shelf_height + height_offset + catch_lenght * sin(90 - catch_angle);

    //Tall support
    translate([0, 0, -total_height])
        cube(size = [thickness, width, total_height], center = false);

    //Catch
    translate([0, 0 , -total_height + height_offset])
        rotate([0, catch_angle, 0])
            cube(size = [thickness, width, catch_lenght], center = false);
}

module main(
    length = 15,
    width = 12,
    thickness = 2,
    peg_diameter = 6,
    peg_depth = 5.5,
    shelf_height = 13.5,
    brace_type = "single"
)
{
    support(length, width, thickness);
    brace_type(length, width, thickness, brace_type);
    peg(width, thickness, peg_diameter, peg_depth);
    catch(width, thickness, shelf_height);
}

main();
